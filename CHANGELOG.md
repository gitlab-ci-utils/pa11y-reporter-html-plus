# Changelog

## Unreleased

### Changed

- BREAKING: Deprecated support for Node 21 (end-of-life 2024-06-01) and added
  support for Node 22 (released 2024-04-25). Compatible with all current and
  LTS releases (`^18.12.0 || ^20.9.0 || >=22.0.0`). (#45)

## v4.0.1 (2024-04-10)

### Fixed

- Fixed keyboard support for copy selector buttons. (#44)

## v4.0.0 (2024-03-27)

### Changed

- BREAKING: Updated reporter to require `pa11y` v8.

### Miscellaneous

- Updated CI pipeline to require report build and test before publish. (#41)
- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#42)

## v3.0.0 (2024-01-16)

### Changed

- BREAKING: Updated support to require `pa11y` v7. (#38)
- BREAKING: Deprecated support for Node 16 (end-of-life 2023-09-11) and added
  support for Node 21 (released 2023-10-17). Compatible with all current and
  LTS releases (^18.12.0 || >=20.0.0). (#36, #37)
- BREAKING: Updated package `main` to `exports`, so only the default export is
  available.
- Updated report CSS to use logical properties.
  ([!266](https://gitlab.com/gitlab-ci-utils/pa11y-reporter-html-plus/-/merge_requests/266))

### Fixed

- Updated visual regression tests to replace report date with a fixed value to
  remove differences and adjusted pixel threshold. (#39)

## v2.0.1 (2023-08-13)

### Fixed

- Updated to latest dependencies (`handlebars@4.7.8`).

### Miscellaneous

- Updated documentation to clarify requirements on `pa11y` and reporter
  installation location.

## v2.0.0 (2023-06-08)

### Changed

- BREAKING: Deprecated support for Node 14 (end-of-life 2023-04-30) and 19
  (end-of-life 2023-06-01). Compatible with all current and LTS releases
  (^16.13.0 || ^18.12.0 || >=20.0.0). (#30)
- Updated report to use system fonts.

### Miscellaneous

- Migrated Playwright tests from Ubuntu Focal to Jammy. (#31)

## v1.1.1 (2023-03-07)

### Fixed

- Fixed error in generating link from `htmlcs` issues with special
  formatting. (#28)
- Fixed keyboard accessibility issues with report filter buttons. (#27)

## v1.1.0 (2023-02-26)

### Changed

- Added buttons to accessibility issues to copy the selector to the clipboard
  to more easily locate the element on the page (visible when hovering over an
  issue). (#5)

### Fixed

- Updated UI tests to use preferred locators added in Playwright v1.27. (#24)

### Miscellaneous

- Updated the `build_report` job to fail if incorrect results found, which can occur
  if there are network errors when downloading page assets. Otherwise, the job
  passes and the `tests-ui` job fails instead. (#19).

## v1.0.5 (2023-02-14)

### Fixed

- Updated reporter to not raise an error when the `axe` runner reports a `null`
  value for `impact`. This should not occur, but erroneously does
  [in some cases](https://github.com/dequelabs/axe-core/issues/3913). The report
  will show `Null` for the impact in these cases. (#26)

## v1.0.4 (2023-02-05)

### Fixed

- Refactored code for changes in latest `eslint` rules.
- Refactored tests for changes in `jest` v29.

### Miscellaneous

- Added browser-based testing to CI pipeline, including visual regression test
  and all report UI interactivity. Updated `renovate` config to combine
  Playwright package and container image updates. (#6, #18, #20, #21, #22)
- Updated pipeline to have long-running jobs use GitLab shared `medium` sized
  runners. (#23)

## v1.0.3 (2022-07-19)

### Fixed

- Fixed error in link for `htmlcs` issues where the `code` contains a standard since different severity (e.g. G141 is a warning a level A/AA, but an error at level AAA). (#17)

## v1.0.2 (2022-07-10)

### Fixed

- Fixed minor formatting issues in the errors/warnings/notices count buttons. (#15, #16)

## v1.0.1 (2022-07-06)

### Fixed

- Removed `pa11y` from `peerDependencies` since the reporter provides a property that Pa11y uses to identify compatibility. (#14)

## v1.0.0 (2022-07-04)

Initial release
