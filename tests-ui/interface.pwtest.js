'use strict';

const { test, expect } = require('@playwright/test');
const { pathToFileURL } = require('node:url');

const reportFileName = './report.html';
const reportUrl = pathToFileURL(reportFileName).toString();

const issueCounts = {
    errors: 87,
    notices: 143,
    warnings: 88
};
const totalIssueCount =
    issueCounts.errors + issueCounts.warnings + issueCounts.notices;

const issuesRole = 'listitem';

const testCases = [
    {
        label: `${issueCounts.errors} errors`,
        title: /^Error.*$/,
        type: 'errors'
    },
    {
        label: `${issueCounts.warnings} warnings`,
        title: /^Warning.*$/,
        type: 'warnings'
    },
    {
        label: `${issueCounts.notices} notices`,
        title: /^Notice.*$/,
        type: 'notices'
    }
];

test.describe('report filter tests', () => {
    test('should open with all issues displayed', async ({ page }) => {
        await page.goto(reportUrl);
        await expect(page.getByRole(issuesRole)).toHaveCount(totalIssueCount);
    });

    for (const { type, label, title } of testCases) {
        test(`should toggle ${type} when ${type} filter clicked`, async ({
            page
        }) => {
            // Go to page and confirm that all issues are visible
            await page.goto(reportUrl);
            await expect(page.getByRole(issuesRole)).toHaveCount(
                totalIssueCount
            );

            // Apply filter then check that issues are correct issues are
            // visible and filtered out issues are not visible.
            await page.getByText(label).click();
            await expect(page.getByRole(issuesRole)).toHaveCount(
                totalIssueCount - issueCounts[type]
            );
            await expect(
                /* eslint-disable-next-line playwright/no-raw-locators -- bet
                   way to select visible elements */
                page.getByText(title).locator('visible=true')
            ).toHaveCount(0);

            // Remove filter then check that all issues are visible.
            await page.getByText(label).click();
            await expect(page.getByRole(issuesRole)).toHaveCount(
                totalIssueCount
            );
        });
    }

    test('should display copy to clipboard button when hovering over an issue', async ({
        page
    }) => {
        const issueNumber = 0;
        await page.goto(reportUrl);
        /* eslint-disable-next-line playwright/no-nth-methods -- select an
           arbitrary issue since all the same */
        const firstIssue = page.getByRole(issuesRole).nth(issueNumber);
        const firstIssueButton = firstIssue.getByRole('button');

        await firstIssue.hover();
        await expect(firstIssueButton).toBeVisible();

        await page
            .getByRole(issuesRole)
            /* eslint-disable-next-line playwright/no-nth-methods -- select a
               different arbitrary issue so original loses hover */
            .nth(issueNumber + 1)
            .hover();
        await expect(firstIssueButton).toBeHidden();
    });
});
