'use strict';

const { pathToFileURL } = require('node:url');
const { test, expect } = require('@playwright/test');

const reportFileName = './report.html';
const reportUrl = pathToFileURL(reportFileName).toString();

// Disable screenshot on failure since saved as part of screenshot test.
// Disable trace since no interaction with the page.
test.use({ screenshot: 'off', trace: 'off' });

test.describe('visual regression tests', () => {
    test('should match page screenshot', async ({ page }) => {
        await page.goto(reportUrl);

        // Update page date to a fix value to avoid date differences in screenshot
        await page.evaluate(() => {
            // eslint-disable-next-line no-undef -- Running in browser context
            const h2 = document.querySelector('h2');
            h2.textContent =
                'Generated at: Fri Aug 11 2023 13:35:11 GMT-0700 (Mountain Standard Time)';
        });

        // Allow some pixel difference for rendering variations. Expanded to
        // 1% to account for rendering differences in webkit.
        await expect(page).toHaveScreenshot({ maxDiffPixelRatio: 0.01 });
    });
});
