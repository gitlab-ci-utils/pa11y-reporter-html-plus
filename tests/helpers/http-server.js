'use strict';

/**
 * Creates an HTTP server for local testing.
 *
 * @module http-server
 */

const http = require('node:http');
const handler = require('serve-handler');

const port = 8080;
const options = {
    // Root directory of server, relative to cwd for tests
    public: 'tests/fixtures/site'
};

/**
 * Starts an http server for local testing. The callback function is
 * passed a reference to the server.
 *
 * @param {Function} done Callback function.
 * @static
 */
const startServer = (done) => {
    const server = http.createServer(
        { requestTimeout: 120_000 },
        (request, response) => handler(request, response, options)
    );

    // eslint-disable-next-line promise/prefer-await-to-callbacks -- http API
    server.listen(port, (error) => {
        done(error, server);
    });
};

module.exports.startServer = startServer;
