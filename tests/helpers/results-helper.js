'use strict';

/**
 * Test helper functions to manipulate Pa11y results.
 *
 * @module results-helper
 */

const fs = require('node:fs');
const path = require('node:path');

const testResultsPath = './tests/fixtures/results/';

/**
 * Loads Pa11y JSON results from a file and returns the resulting object.
 *
 * @param   {string} fileName The results filename.
 * @returns {object}          The results object.
 * @static
 * @public
 */
const getResultsFromFile = (fileName) =>
    JSON.parse(fs.readFileSync(path.join(testResultsPath, fileName), 'utf8'));

module.exports.getResultsFromFile = getResultsFromFile;
