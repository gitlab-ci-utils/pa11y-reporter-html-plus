'use strict';

const reporter = require('..');
const { getResultsFromFile } = require('./helpers/results-helper');

const semverValidRange = require('semver/ranges/valid');

describe('pa11y reporter interface', () => {
    describe('supports', () => {
        it('should be a string', () => {
            expect.assertions(1);
            expect(typeof reporter.supports).toBe('string');
        });

        it('should be a valid semver range', () => {
            expect.assertions(1);
            expect(semverValidRange(reporter.supports)).not.toBeNull();
        });
    });

    describe('results', () => {
        it('should be a function', () => {
            expect.assertions(1);
            expect(typeof reporter.results).toBe('function');
        });

        it('should return a string', async () => {
            expect.assertions(1);
            const pa11yResults = {
                documentTitle: 'foo',
                issues: [],
                pageUrl: 'foo.html'
            };
            const result = await reporter.results(pa11yResults);
            expect(typeof result).toBe('string');
        });

        const reportTestCases = [
            {
                documentTitle: 'foo with htmlcs issues',
                issues: getResultsFromFile('page-with-errors-htmlcs.json'),
                name: 'if issues found with htmlcs',
                pageUrl: 'foo-with-htmlcs-issues.html'
            },
            {
                documentTitle: 'foo with axe issues',
                issues: getResultsFromFile('page-with-errors-axe.json'),
                name: 'if issues found with axe',
                pageUrl: 'foo-with-axe-issues.html'
            },
            {
                documentTitle: 'foo no issues',
                issues: [],
                name: 'if no issues found',
                pageUrl: 'foo-no-issues.html'
            },
            {
                documentTitle: 'foo with no context or selector',
                issues: getResultsFromFile(
                    'page-with-errors-htmlcs-no-context-selector.json'
                ),
                name: 'if issues found with no context or selector',
                pageUrl: 'foo-with-no-context-selector.html'
            }
        ];

        it.each(reportTestCases)(
            'should return the rendered HTML report $name',
            async ({ issues, documentTitle, pageUrl }) => {
                expect.assertions(1);
                const pa11yResults = { documentTitle, issues, pageUrl };
                const results = await reporter.results(pa11yResults);
                // Report includes the date, which varies, so remove
                const sanitizedResults = results.replace(
                    /<h2>Generated at:.*?<\/h2>/,
                    ''
                );
                expect(sanitizedResults).toMatchSnapshot();
            }
        );
    });

    describe('error', () => {
        it('should be a function', () => {
            expect.assertions(1);
            expect(typeof reporter.error).toBe('function');
        });

        it('should return the original error message', () => {
            expect.assertions(1);
            const errorMessage = 'This is an error';
            expect(reporter.error(errorMessage)).toBe(errorMessage);
        });
    });
});
