'use strict';

const { promisify } = require('node:util');
const exec = promisify(require('node:child_process').exec);
const { startServer } = require('./helpers/http-server');

// Longer timeout needed when generating report (default 5s).
// Increased to 120s since 60s was timing out for Windows CI jobs.
const reportTimeout = 120_000;
jest.setTimeout(reportTimeout);

const urlNoIssues = 'http://localhost:8080/page-no-errors.html';
const urlWithIssues = 'http://localhost:8080/page-with-errors.html';

const integrationTestCases = [
    { issues: 'a11y issues', runner: 'htmlcs', url: urlWithIssues },
    { issues: 'a11y issues', runner: 'axe', url: urlWithIssues },
    { issues: 'no issues', runner: 'htmlcs', url: urlNoIssues }
];

describe('integration tests', () => {
    // eslint-disable-next-line jest/no-done-callback -- startServer API
    beforeAll((done) => {
        // eslint-disable-next-line promise/prefer-await-to-callbacks -- startServer API
        startServer((error, server) => {
            if (!error) {
                globalThis.server = server;
            }
            done(error);
        });
    });

    afterAll(() => {
        if (globalThis.server) {
            globalThis.server.close();
        }
    });

    it.each(integrationTestCases)(
        'should run pa11y with $runner runner and $issues and generate the expected output',
        async ({ runner, url }) => {
            expect.assertions(1);
            const configFile =
                /* eslint-disable-next-line jest/no-conditional-in-test --
                   unique config for Windows running as SYSTEM, see #46 */
                process.platform === 'win32'
                    ? '--config="./tests/win.pa11y.json"'
                    : '';
            const command = `pa11y --reporter="./index.js" --runner="${runner}" --timeout=120000 ${configFile} ${url} || exit 0`;

            const result = await exec(command);

            // Report includes the date, which varies, so remove
            const sanitizedResults = result.stdout.replace(
                /<h2>Generated at:.*?<\/h2>/,
                ''
            );
            expect(sanitizedResults).toMatchSnapshot();
        }
    );

    it('should run pa11y with an execution error and generate the expected output', async () => {
        expect.assertions(1);
        // The TLD ".invalid" in intended for testing to ensure it never resolves
        const url = 'https://this.is.invalid';
        const configFile =
            /* eslint-disable-next-line jest/no-conditional-in-test --
           unique config for Windows running as SYSTEM, see #46 */
            process.platform === 'win32'
                ? '--config="./tests/win.pa11y.json"'
                : '';
        const command = `pa11y --reporter="./index.js" --timeout=120000 ${configFile} ${url} || exit 0`;

        const result = await exec(command);

        expect(result.stderr).toMatch(`ERR_NAME_NOT_RESOLVED at ${url}`);
    });
});
