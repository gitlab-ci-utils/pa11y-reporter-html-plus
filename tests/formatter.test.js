'use strict';

const { formatIssue, getIssuesSummary } = require('../lib/formatter');
const { getResultsFromFile } = require('./helpers/results-helper');

const axeResultsFileName = 'page-with-errors-axe.json';
const axeResultsNullImpactFileName = 'page-with-errors-axe-null-impact.json';
const htmlcsResultsFileName = 'page-with-errors-htmlcs.json';
const htmlcsEwnResultsFileName = 'page-with-errors-htmlcs-EWN.json';
const htmlcsNoUrlResultsFileName = 'page-with-errors-htmlcs-no-url.json';
const htmlcsStandardResultsFileName =
    'page-with-errors-htmlcs-with-standard.json';
const htmlcsAxeResultsFileName = 'page-with-errors-htmlcs-axe.json';
const genericResultsFileName = 'page-with-errors-other.json';

// Preload to avoid repeating through test suite
const axeResults = getResultsFromFile(axeResultsFileName);
const axeResultsNullImpact = getResultsFromFile(axeResultsNullImpactFileName);
const htmlcsResults = getResultsFromFile(htmlcsResultsFileName);
const htmlcsEwnResults = getResultsFromFile(htmlcsEwnResultsFileName);
const htmlcsNoUrlResults = getResultsFromFile(htmlcsNoUrlResultsFileName);
const htmlcsStandardResults = getResultsFromFile(htmlcsStandardResultsFileName);
const htmlcsAxeResults = getResultsFromFile(htmlcsAxeResultsFileName);
const genericResults = getResultsFromFile(genericResultsFileName);

const formatIssueTestCases = [
    {
        pa11yResults: axeResults,
        testName: 'axe runner results'
    },
    {
        pa11yResults: axeResultsNullImpact,
        testName: 'axe runner results with null impact'
    },
    {
        pa11yResults: htmlcsResults,
        testName: 'htmlcs runner results'
    },
    {
        pa11yResults: htmlcsNoUrlResults,
        testName: 'htmlcs runner results if helpUrl not found'
    },
    {
        pa11yResults: htmlcsStandardResults,
        testName: 'htmlcs runner results if code includes standard (e.g. G141)'
    },
    {
        pa11yResults: genericResults,
        testName: 'results for unknown runner'
    },
    {
        pa11yResults: htmlcsAxeResults,
        testName: 'multiple runner results'
    }
];

describe('format issue', () => {
    it.each(formatIssueTestCases)(
        `should return properly formatted $testName`,
        ({ pa11yResults }) => {
            expect.assertions(1);
            const formattedResults = pa11yResults.map((issue) =>
                formatIssue(issue)
            );

            expect(formattedResults).toMatchSnapshot();
        }
    );
});

const countsTestCases = [
    {
        expectedCounts: { errors: 2, notices: 25, warnings: 1 },
        name: 'all types',
        results: htmlcsEwnResults
    },
    {
        expectedCounts: { errors: 2, notices: 0, warnings: 0 },
        name: 'only errors',
        results: htmlcsResults
    },
    {
        expectedCounts: { errors: 0, notices: 0, warnings: 0 },
        name: 'no issues',
        results: []
    },
    {
        expectedCounts: { errors: 0, notices: 0, warnings: 0 },
        name: 'undefined issues'
    }
];

const runnersTestCases = [
    {
        expectedRunners: ['htmlcs'],
        name: 'htmlcs issues',
        results: htmlcsResults
    },
    {
        expectedRunners: ['axe'],
        name: 'axe issues',
        results: axeResults
    },
    {
        expectedRunners: ['axe', 'htmlcs'],
        name: 'htmlcs and axe issues',
        results: htmlcsAxeResults
    },
    {
        expectedRunners: ['weekday'],
        name: 'unknown runner issues',
        results: genericResults
    },
    {
        expectedRunners: [],
        name: 'no issues',
        results: []
    },
    {
        expectedRunners: [],
        name: 'undefined issues'
    }
];

describe('get issues summary', () => {
    describe('issue counts', () => {
        it.each(countsTestCases)(
            'should return the correct issue counts with $name',
            ({ results, expectedCounts }) => {
                expect.assertions(3);
                const { counts } = getIssuesSummary(results);
                expect(counts.errors).toBe(expectedCounts.errors);
                expect(counts.warnings).toBe(expectedCounts.warnings);
                expect(counts.notices).toBe(expectedCounts.notices);
            }
        );
    });

    describe('issue runners', () => {
        it.each(runnersTestCases)(
            'should return the runners with $name',
            ({ results, expectedRunners }) => {
                expect.assertions(1);
                const { runners } = getIssuesSummary(results);
                expect(runners.sort()).toStrictEqual(expectedRunners);
            }
        );
    });
});
