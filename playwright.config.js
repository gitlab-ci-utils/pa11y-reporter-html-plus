// @ts-check
'use strict';

const { devices } = require('@playwright/test');

const isCI = Boolean(process.env.CI);

/**
 * @type {import('@playwright/test').PlaywrightTestConfig}
 * @see https://playwright.dev/docs/test-configuration
 */
const config = {
    expect: {
        // eslint-disable-next-line no-magic-numbers -- intuitive
        timeout: 5 * 1000
    },
    fullyParallel: true,
    outputDir: 'test-results/',
    projects: [
        { name: 'chromium', use: { ...devices['Desktop Chrome'] } },
        { name: 'firefox', use: { ...devices['Desktop Firefox'] } },
        { name: 'webkit', use: { ...devices['Desktop Safari'] } }
    ],
    reporter: [
        ['list'],
        ['html', { open: 'never', outputFolder: 'playwright-report/' }],
        ['junit', { outputFile: 'playwright-junit.xml' }]
    ],
    retries: 0,
    testDir: './tests-ui',
    testMatch: /.*\.pwtest\.(?:js|ts)/,
    // eslint-disable-next-line no-magic-numbers -- intuitive
    timeout: 30 * 1000,
    // Disable parallel tests in CI
    use: {
        launchOptions: {
            chromiumSandbox: !isCI
        },
        screenshot: 'only-on-failure',
        trace: 'retain-on-failure'
    },
    workers: isCI ? 1 : undefined
};

module.exports = config;
