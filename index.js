'use strict';

const fs = require('node:fs').promises;
const path = require('node:path');
const handlebars = require('handlebars');
const { formatIssue, getIssuesSummary } = require('./lib/formatter');

const reportTemplateDirectory = 'lib';
const reportTemplateName = 'index.handlebars';

/**
 * Implements the supports function from the reporter API.
 * Indicated compatible versions of Pa11y.
 */
const supports = '^8.0.0 || ^8.0.0-alpha || ^8.0.0-beta';

/**
 * Implements the results function from the reporter API.
 * Generates the HTML report for the results and returns to Pa11y.
 *
 * @param   {object} results The Pa11y results.
 * @returns {string}         The HTML report for the given results as a string.
 * @static
 * @public
 */
// eslint-disable-next-line no-shadow -- following API nomenclature
const results = async (results) => {
    const { counts, runners } = getIssuesSummary(results.issues);
    const formattedResults = {
        counts,
        date: new Date(),
        documentTitle: results.documentTitle,
        issues: results.issues.map((issue) => formatIssue(issue)),
        pageUrl: results.pageUrl,
        runners: runners.length > 0 ? runners.join(', ') : 'none'
    };

    const templateFile = path.resolve(
        __dirname,
        reportTemplateDirectory,
        reportTemplateName
    );
    const reportTemplate = await fs.readFile(templateFile, 'utf8');
    const template = handlebars.compile(reportTemplate);
    return template(formattedResults);
};

/**
 * Implements the error function from the reporter API.
 * Errors are echoed back for handling by Pa11y.
 *
 * @param   {string} message The Pa11y error.
 * @returns {string}         The Pa11y error.
 * @static
 * @public
 */
const error = (message) => message;

module.exports = {
    error,
    results,
    supports
};
