# Pa11y HTML Reporter Plus

A [Pa11y](https://github.com/pa11y/pa11y) reporter that generates an HTML
report with enhanced support for all runners, including links to relevant
help, and the ability to filter accessibility issues by type.

## HTML reports

Pa11y HTML Reporter Plus generates an HTML report, similar to the included
Pa11y HTML report, but with the following differences:

- For the included Pa11y runners (`htmlcs` and `axe`), each issue includes a
  link to the relevant documentation.
- Accessibility issues can be toggled by Pa11y type (`error`, `warning`,
  `notice`).
- For the `axe` runner, lists the Axe `impact` in addition to the Pa11y type.
  Color coding and filtering is still by the Pa11y type.
  - Note that Pa11y makes some assumptions when converting Axe
    `violations`/`incomplete` results and their `impact` to Pa11y types, which
    aren't altered.
- Lists the runners that identified issues (unfortunately the reporter doesn't
  know the runners that executed, so they're not all listed, only those
  reporting issues).

An example report from Pa11y analysis of a page from the
[W3C accessibility demo site](https://www.w3.org/WAI/demos/bad/before/home.html)
is available
[here](https://gitlab-ci-utils.gitlab.io/pa11y-reporter-html-plus/report.html).
This includes results from both the `htmlcs` and `axe` runners.

## Installation

Install Pa11y HTML Reporter Plus via
[npm](https://www.npmjs.com/package/pa11y-reporter-html-plus).

```sh
npm install pa11y-reporter-html-plus
```

Note: `pa11y` and `pa11y-reporter-html-plus` must either both be installed
globally or both be installed locally. Using a mixed configuration can result
in the reporter not being found.

## Usage

Pa11y HTML Reporter Plus is a Pa11y compatible reporter. As with all reporters,
it can be specified via the CLI or a Pa11y configuration file. Complete details
on using reporters are available in the
[Pa11y documentation](https://github.com/pa11y/pa11y#reporters).

### Using via CLI

The reporter is used with Pa11y via the CLI as follows:

```sh
pa11y --reporter=html-plus https://pa11y.org/
```

Note that Pa11y assumes all reporter packages are named in the form
`pa11y-reporter-<reporter name>`, so only `html-plus` specified.

Pa11y outputs reporter results to `stdout` so in most cases from the CLI the
results are piped to a file.

```sh
pa11y --reporter=html-plus https://pa11y.org/ > report.html
```

### Using via configuration file

You can specify reporters in a Pa11y configuration file in the `reporter`
property.

```json
{
  "reporter": "html-plus"
}
```

As noted in the preceding example, Pa11y assumes all reporter packages are
named in the form `pa11y-reporter-<reporter name>`, so only `html-plus`
specified.
